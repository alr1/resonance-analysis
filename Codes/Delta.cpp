#include <iostream>
#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using std::endl;
using std::vector; 		using std::string;
using std::ofstream; 	using std::ifstream;

// Estrutura responsável por guardar os arquivos em formato lista encadeada
struct t_data
{
	double freq;
	vector<double> real;
	vector<double> imag;
};

// Estrutura de encadeamento -> nodo
struct t_nodo
{
	t_data data;
	struct t_nodo *prox;
};

// Estrutura de lista que aponta para uma das estruturas de dados
struct t_lista_enc
{
	t_nodo *ini;
};

void inicializar(t_lista_enc *);
void inserir(t_lista_enc *, t_data);
void alocar(t_lista_enc *, double, double, int, int);
int max_negative_field(t_lista_enc *, int, const vector<double> &);
int min_negative_field(t_lista_enc *, int, const vector<double> &, int);
int max_positive_field(t_lista_enc *, int, const vector<double> &, int);
int min_positive_field(t_lista_enc *, int, const vector<double> &, int);
void print_min(t_lista_enc *, int, ofstream &, int, int, int, int, const vector<double> &);
void print_freq(t_lista_enc *, int, int, const vector<double> &, string&, string&);
int max_neg(t_lista_enc *, int, const vector<double> &);
int max_pos(t_lista_enc *, int, const vector<double> &, int);
void print_max(t_lista_enc *, int, ofstream &, int, int, const vector<double> &);


int main(int argc, char** argv)
{
	int i, j, k, m, size, count = 0, flag = 0, max1, min1, max2, min2;
	double campo, teste, teste2, lixo2, lixo3;
	char ch;
	string nome, arquivo1, lixo, gcheck, path, path2;
	t_lista_enc *l = new t_lista_enc;
	t_data novo;
	vector<double> field;

/*<=================================================================================================================>*/

	// Inicializa a lista encadeada
	inicializar(l);

/*<=================================================================================================================>*/

	// Do-While responsável por detectar problemas no arquivo
	do
	{
		// Obter o nome do arquivo que será trabalhado
		arquivo1 = argv[1];

		// Obter a pasta do arquivo da amostra para salvar os resultados
		path = argv[1];
		lixo = path.back();

		#ifdef _WIN32
		{
			while(lixo != "\\")
			{
				path.pop_back();
				lixo = path.back();
			}

			path = path + "Variation_Results\\";

			path2 = path;					// path2 será usado pelo C++ para abrir arquivos
			
			system(("mkdir \"" + path2 + "\"").c_str());
		}
		#endif

		#ifdef linux
		{
			while(lixo != "/")
			{
		 		path.pop_back();
			 	lixo = path.back();
			}

			path = path + "Variation_Results/";

			path2 = path;					// path2 será usado pelo C++ para abrir arquivos

			// Remover espaços para usar comandos do terminal "system()" no linux
			i = path.find(" ");
			if(i != std::string::npos)
			{
				while(path.find(" ", i) != std::string::npos)
				{
					path.insert(path.find(" ", i), "\\");
					i = path.find(" ", i)+2;
				}
			}

			// Diretório com os resultados
			system(("mkdir " + path).c_str());
		}
		#endif

/*<=================================================================================================================>*/

		// If responsável por detectar se o arquivo existe ou não
		if(access(arquivo1.c_str(), F_OK) != -1)
		{
			// Abre o arquivo para leitura
			ifstream arq1(arquivo1.c_str());

			i = 0;
			count = 0;

			// Descarta a primeira linha do arquivo -> cabeçalho
			getline(arq1, lixo);

			// Laço responsável por ler o arquivo e detectar o número de frequências criando estruturas na lista
			while(arq1)
			{
				arq1 >> teste;	arq1 >> ch;		// Guarda a frequência em "teste". "ch" serve para descartar as vírgulas das colunas
				arq1 >> campo;

				// Salva o primeiro valor de campo em "teste2" para comparação
				if(flag == 0)
				{
					teste2 = campo;
					flag++;
				}

				// O arquivo é organizado por ordem de campo. Esse if detecta quando o campo muda e, portanto, todas frequencias foram salvas
				if(campo == teste2)
				{
					// Função para criar a estrutura com a frequência como característica
					novo.freq = teste;
					inserir(l, novo);

					// Laço para descartar as outras colunas do arquivo
					for(j = 2; j < 5; j++)
					{
						arq1 >> ch;	arq1 >> lixo2;
					}

					// "count" está contando o número de frequências
					count++;
					i++;
				}

				else
					break;
			}
			arq1.close();

/*<=================================================================================================================>*/

			// "m" será a variável responsável por guardar o número de frequências com que estamos lidando
			m = count;

			// Abrimos o arquivo para contar o número de linhas
			arq1.open(arquivo1.c_str());

			count = 0;

			// Laço responsável por contar a quantidade de linhas no arquivo
			while(getline(arq1, lixo))
				count++;

			arq1.close();

			// Size será a variável responsável por guardar o número de valores de campo utilizados
			size = (count-1)/m;

			// Flag a partir daqui será a variável com o número de frequências
			flag = m;

/*<=================================================================================================================>*/
/*														     */
/*<=================================================================================================================>*/

			// Abrimos dessa vez o arquivo para salvar as partes reais e imaginárias das medidas
			arq1.open(arquivo1.c_str());

			// Eliminar a primeira linha do arquivo
			getline(arq1, lixo);

			// Resetar as variáveis
			i = 0;
			count = 0;
			lixo3 = RAND_MAX;		// "lixo3" serve para detectarmos quando o valor de campo mudou

	/*<=================================================================================================================>*/

			// Laço responsável por varrer o arquivo buscando os dados
			while(arq1)
			{
				arq1 >> lixo2;	arq1 >> ch;
				arq1 >> campo;	arq1 >> ch;

				// Condicional para guardar os valores de campo no vetor
				if(campo != lixo3)
				{
					field.push_back(campo);
					i++;
					lixo3 = campo;
				}

				// Salvamos as partes reais e imaginárias nas variáveis "teste" e "teste2" para serem passadas para a função "alocar"
				arq1 >> teste; 	arq1 >> ch;
				arq1 >> teste2;	arq1 >> ch;

				// Descarta a última coluna
				arq1 >> lixo2;

				// Função para salvar os valores em seus respectivos vetores nas estruturas de dados
				alocar(l, teste, teste2, count%flag, i-1);

				count++;
			}

			arq1.close();

/*<=================================================================================================================>*/

			// Arquivo em que será salvo os dados de largura de linha
			ofstream arq2((path2 + "Linewidth.dat").c_str());

			arq2 << "Freq\tLinewidth(Oe-)\tLinewidth(Oe+)" << endl;		// Cabeçalho
			arq2 << std::fixed;						// Comando para evitar o C++ de escrever em notação científica

			// Laço para varrer cada uma das frequências
			for(i = 0; i < flag; i++)
			{
				// Funções responsáveis por encontrar o índice em que estão os mínimos e máximos da frequência (se existem)
				max1 = max_negative_field(l, i, field);
				if(max1 != RAND_MAX)
					min1 = min_negative_field(l, i, field, max1);

				max2 = max_positive_field(l, i, field, size);
				if(max2 != RAND_MAX)
					min2 = min_positive_field(l, i, field, max2);

				// Condicional para decidir se há mínimos e máximos
				if(max2 == RAND_MAX || max1 == RAND_MAX || min2 == RAND_MAX || min1 == RAND_MAX)
					;

				else if(fabs(field[max1]-field[min1]) > 100 || fabs(field[max2]-field[min2]) > 100)
					;

				else
					// Função para imprimir as larguras de linha
					print_min(l, i, arq2, min1, min2, max1, max2, field);
			}

			arq2.close();

/*<=================================================================================================================>*/

			// Arquivo em que será salvo os dados de máximos da parte real, que coincidem com a ressonância
			arq2.open((path2 + "Resonance.dat").c_str());
			arq2 << "Freq\tField\tMax" << endl;

			// Laço para varrer cada uma das frequências
			for(i = 0; i < flag; i++)
			{
				// Funções responsáveis por encontrar o índice em que estão os máximos da frequência (se existem)
				max1 = max_neg(l, i, field);
				max2 = max_pos(l, i, field, size);

				if(max2 == RAND_MAX || max1 == RAND_MAX)
					// Ignorar quando não há máximos
					;
				else
					// Função para imprimir onde estão os máximos e escrever no arquivo para obter a ressonância
					print_max(l, i, arq2, max1, max2, field);
			}

			arq2.close();

		}

	} while(access(arquivo1.c_str(), F_OK) == -1);

/*<=================================================================================================================>*/
/*														     */
/*<=================================================================================================================>*/

	// Função para printar resultados em função do campo
	print_freq(l, flag, size, field, path, path2);

	// Liberar a memória e deletar a lista encadeada
	delete l;

	return 0;
}


/*<=================================================================================================================>*/
/*<=================================================================================================================>*/
/*<=================================================================================================================>*/


// Inicializar a lista
void inicializar(t_lista_enc *l)
{
  	l->ini = NULL;
}

/*<=================================================================================================================>*/

// Função para inserir novas estruturas na lista encadeada
void inserir(t_lista_enc *l, t_data dado)
{
  	t_nodo *novo = NULL;
  	t_nodo *ant = NULL;
  	t_nodo *aux = l->ini;
  	novo = new t_nodo;

  	novo->data = dado;

  	while((aux != NULL))
  	{
    		ant = aux;
		aux = aux->prox;
  	}

	if(ant == NULL)
  	{
    		novo->prox = l->ini;
    		l->ini = novo;
  	}

  	else
  	{
    		novo->prox = ant->prox;
    		ant->prox = novo;
  	}
}

/*<=================================================================================================================>*/

// Função para salvar os dados do arquivo em vetores na lista encadeada
void alocar(t_lista_enc *l, double real, double imag, int j, int k)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < j; i++)
		aux = aux->prox;

	aux->data.real.push_back(real);
	aux->data.imag.push_back(imag);
}

/*<=================================================================================================================>*/

// Função para encontrar o máximo para campos negativos
int max_negative_field(t_lista_enc *l, int k, const vector<double> &field)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, max = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(h = 1; field[h] < 0; h++)
	{
		d1 = (aux->data.imag[h+1]-aux->data.imag[h-1])/(2*dh);

		// Condicional que detecta derivada positiva por se aproximar de um máximo
		if(d1 > 0)
			flag2++;
		else
			flag2 = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag2 == 6)
		{
			// Laço para varrer os dados com campo negativo
			for(i = h-5; field[i] < 0; i++)
			{
				d2 = (aux->data.imag[i+1]-aux->data.imag[i-1])/(2*dh);

				// Condicional que detecta derivada negativa por passar de um máximo
				if(d2 < 0)
					flag++;
				else
					flag = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag == 6)
				{
					max = i-12;

					// Laço para achar o máximo
					for(j = i-20; j < i; j++)
						if(aux->data.imag[j] > aux->data.imag[max])
							max = j;
					break;
				}
			}
			break;
		}
	}
	return max;
}

/*<=================================================================================================================>*/

// Função para encontrar o mínimo para campos negativos
int min_negative_field(t_lista_enc *l, int k, const vector<double> &field, int max)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, flag2 = 0, min, teste = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(i = max; field[i] < 0; i++)
	{
		d1 = (aux->data.imag[i+1]-aux->data.imag[i-1])/(2*dh);

		// Condicional que detecta derivada positiva por passar de um mínimo
		if(d1 > 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 5)
		{
			for(j = i-3; j < i+20; j++)
			{
				d2 = (aux->data.imag[j+1]-aux->data.imag[j-1])/(2*dh);

				// Condicional que detecta derivada negativa por se aproxima de um decréscimo
				if(d2 < 0)
					flag2++;
				else
					flag2 = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag2 == 5)
				{
					teste = j-5;
					break;
				}
			}
			min = max;

			if(teste == RAND_MAX)
			{
				// Laço para achar o mínimo sem decréscimo pós mínimo
				for(j = max; j < i+20; j++)
					if(aux->data.imag[j] < aux->data.imag[min])
						min = j;
				break;
			}

			else
			{
				// Laço para achar o mínimo com decréscimo pós mínimo
				for(j = max; j < teste; j++)
					if(aux->data.imag[j] < aux->data.imag[min])
						min = j;
				break;
			}
		}
	}

	if(min != max)
		return min;
	else
		return RAND_MAX;
}

/*<=================================================================================================================>*/

// Função para encontrar o máximo para campos positivos
int max_positive_field(t_lista_enc *l, int k, const vector<double> &field, int size)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, max = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(h = size-2; field[h] > 0; h--)
	{
		d1 = (aux->data.imag[h+1]-aux->data.imag[h-1])/(2*dh);

		// Condicional que detecta derivada negativa por se aproximar de um máximo
		if(d1 < 0)
			flag2++;
		else
			flag2 = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag2 == 6)
		{
			// Laço para varrer os dados com campo negativo
			for(i = h+5; field[i] > 0; i--)
			{
				d2 = (aux->data.imag[i+1]-aux->data.imag[i-1])/(2*dh);

				// Condicional que detecta derivada positiva por passar de um máximo
				if(d2 > 0)
					flag++;
				else
					flag = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag == 6)
				{
					max = i+12;

					// Laço para achar o máximo
					for(j = i+20; j > i; j--)
						if(aux->data.imag[j] > aux->data.imag[max])
							max = j;
					break;
				}
			}
			break;
		}
	}
	return max;
}

/*<=================================================================================================================>*/

// Função para encontrar o mínimo para campos positivos
int min_positive_field(t_lista_enc *l, int k, const vector<double> &field, int max)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, flag2 = 0, min, teste = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(i = max; field[i] > 0; i--)
	{
		d1 = (aux->data.imag[i+1]-aux->data.imag[i-1])/(2*dh);

		// Condicional que detecta derivada negativa por passar de um mínimo
		if(d1 < 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 5)
		{
			for(j = i+3; j > i-20; j--)
			{
				d2 = (aux->data.imag[j+1]-aux->data.imag[j-1])/(2*dh);

				// Condicional que detecta derivada positiva por se aproxima de um decréscimo
				if(d2 > 0)
					flag2++;
				else
					flag2 = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag2 == 5)
				{
					teste = j+5;
					break;
				}
			}
			min = max;

			if(teste == RAND_MAX)
			{
				// Laço para achar o mínimo sem decréscimo pós mínimo
				for(j = max; j > i-20; j--)
					if(aux->data.imag[j] < aux->data.imag[min])
						min = j;
				break;
			}

			else
			{
				// Laço para achar o mínimo com decréscimo pós mínimo
				for(j = max; j > teste; j--)
					if(aux->data.imag[j] < aux->data.imag[min])
						min = j;
				break;
			}
		}
	}

	if(min != max)
		return min;
	else
		return RAND_MAX;
}

/*<=================================================================================================================>*/

// Função para printar no arquivo os minimos encontrados
void print_min(t_lista_enc *l, int k, ofstream &arq, int min1, int min2, int max1, int max2, const vector<double> &field)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < k; i++)
		aux = aux->prox;

	arq << aux->data.freq << "\t" << field[min1]-field[max1] << "\t" << field[max2]-field[min2] << endl;
}

/*<=================================================================================================================>*/

// Função para printar as curvas de absorção para cada frequência
void print_freq(t_lista_enc *l, int flag, int size, const vector<double> &field, string &path, string &path2)
{
	int i, j, k;
	float mod;
	string fr;
	t_nodo *aux = l->ini;
	ofstream arq;

	// Criar pasta para quardar os arquivos
	#ifdef _WIN32
	{
		system(("mkdir \"" + path2 + "Frequencies\"").c_str());
	}
	#endif

	#ifdef linux
	{
		system(("mkdir " + path + "Frequencies").c_str());
	}
	#endif

	// Laço para printar os resultados para algumas frequências. 50 frequências serão printadas caso hajam 1000 valores de frequências.
	for(i = 0; i < flag; i += 20)
	{
		// Nome do arquivo
		#ifdef _WIN32
		{
			fr = path2 + "Frequencies\\" + std::to_string(i)+".dat";
		}
		#endif

		#ifdef linux
		{
			fr = path2 + "Frequencies/" + std::to_string(i)+".dat";
		}
		#endif

		arq.open(fr.c_str());
		arq << "Freq\tField\tImag\tReal\tmod" << endl;	// Cambeçalho
		arq << std::fixed;				// Evitar notação científica novamente

		// Laço para printar as partes reais, imaginárias e módulo em função do campo para cada frequência
		for(j = 0; j < size; j++)
		{
			mod = aux->data.imag[j]*aux->data.imag[j] + aux->data.real[j]*aux->data.real[j];

			arq << aux->data.freq << "\t" << field[j] << "\t" << aux->data.imag[j] << "\t" << aux->data.real[j] << "\t" << sqrt(mod) << endl;
		}
		arq.close();

		for(k = 0; k < 20; k++)
			aux = aux->prox;
	}
}

/*<=================================================================================================================>*/

// Função para encontrar o máximo para campos negativos
int max_neg(t_lista_enc *l, int k, const vector<double> &field)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, max1 = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	// Laço para varrer os dados com campo negativo
	for(i = 1; field[i] < 0; i++)
	{
		d1 = (aux->data.real[i+1]-aux->data.real[i-1])/(2*dh);

		// Condicional que detecta um aumento no sinal
		if(d1 > 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 6)
		{
			for(j = i-6; field[j] < 0; j++)
			{
				d2 = (aux->data.real[j+1]-aux->data.real[j-1])/(2*dh);

				if(d2 < 0)
					flag2++;
				else
					flag2 = 0;

				if(flag2 == 6)
				{
					max1 = i-6;

					// Laço para achar o máximo
					for(h = i-6; h < j; h++)
						if(aux->data.real[h] > aux->data.real[max1])
							max1 = h;
					break;
				}
			}
			break;
		}
	}

	return max1;
}

/*<=================================================================================================================>*/

// Função para encontrar o máximo para campos positivos
int max_pos(t_lista_enc *l, int k, const vector<double> &field, int size)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, max2 = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	// Laço para varrer os dados com campo positivo
	for(i = size-2; field[i] >= 0; i--)
	{

		d1 = (aux->data.real[i+1]-aux->data.real[i-1])/(2*dh);

		// Condicional que detecta um aumento no sinal
		if(d1 < 0)
		{
			flag++;
		}
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 6)
		{
			for (j = i+6; field[j] >= 0; j--)
			{
				d2 = (aux->data.real[j+1]-aux->data.real[j-1])/(2*dh);

				if (d2 > 0)
					flag2++;
				else
					flag2 = 0;

				if(flag2 == 6)
				{
					max2 = i+6;

					// Laço para achar o máximo
					for(h = i+6; h > j; h--)
						if(aux->data.real[h] > aux->data.real[max2])
							max2 = h;
					break;
				}
			}

			break;
		}
	}

	return max2;
}

/*<=================================================================================================================>*/

// Função para printar no arquivo os máximos da parte real em função da frequência
void print_max(t_lista_enc *l, int k, ofstream &arq1, int max1, int max2, const vector<double> &field)
{
	t_nodo *aux = l->ini;
	arq1 << std::fixed;		// Evitar notação científica

	for(int i = 0; i < k; i++)
		aux = aux->prox;

	arq1 << aux->data.freq << "\t" << field[max1] << "\t" << aux->data.real[max1] << endl;
	arq1 << aux->data.freq << "\t" << field[max2] << "\t" << aux->data.real[max2] << endl;
}
