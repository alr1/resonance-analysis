#include <iostream>
#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <cstring>
#include <string>
#include <iomanip>
#include <complex>
#include <sstream>
#include <algorithm>

using std::endl;
using std::vector; 		using std::string;	using std::complex;
using std::ofstream; 	using std::ifstream;

#define c 299792458.0

// Estrutura responsável por guardar os arquivos em formato lista encadeada
struct t_data
{
	double freq;
	vector<complex<double>> amostra;
	vector<complex<double>> substrato;
	vector<complex<double>> vazio;
	vector<complex<double>> perm_emp;
	vector<complex<double>> perm_subs;
	vector<complex<double>> ueff;
	vector<complex<double>> ur;
};

// Estrutura de encadeamento -> nodo
struct t_nodo
{
	t_data data;
	struct t_nodo *prox;
};

// Estrutura de lista que aponta para uma das estruturas de dados
struct t_lista_enc
{
	t_nodo *ini;
};

void inicializar(t_lista_enc *);
void inserir(t_lista_enc *, t_data);
void alocar(t_lista_enc *, const complex<double> &, const complex<double> &, const complex<double> &, int, int);
complex<double> permampty(t_lista_enc *, double, double, int, int);
complex<double> permsub(t_lista_enc *, double, double, int, int);
complex<double> pereff(t_lista_enc *, double, double, int, int);
void escrever(t_lista_enc *, int, ofstream &, ofstream &, const vector<double> &, int);
void escrever2(t_lista_enc *, int, ofstream &, const vector<double> &, int);
void analise(t_lista_enc *, double, double, int, int, double, double, double);
int max_negative_field(t_lista_enc *, int, const vector<double> &);
int min_negative_field(t_lista_enc *, int, const vector<double> &, int);
int max_positive_field(t_lista_enc *, int, const vector<double> &, int);
int min_positive_field(t_lista_enc *, int, const vector<double> &, int);
int min_negative_field2(t_lista_enc *, int, const vector<double> &);
int max_negative_field2(t_lista_enc *, int, const vector<double> &, int);
int max_positive_field2(t_lista_enc *, int, const vector<double> &, int);
int min_positive_field2(t_lista_enc *, int, const vector<double> &, int);
void print_min(t_lista_enc *, int, ofstream &, int, int, int, int, const vector<double> &);
void print_min2(t_lista_enc *, int, ofstream &, int, int, int, int, const vector<double> &);
int ress_negative_field(t_lista_enc *, int, const vector<double> &, int);
int ress_positive_field(t_lista_enc *, int, const vector<double> &, int);
void print_max(t_lista_enc *, int, ofstream &, int, int, const vector<double> &);

int main(int argc, char** argv)
{
	int i, j, k, m, size, count = 0, flag = 0, max1, min1, max2, min2;
	double campo, lixo2, real, imag, teste, lamostra = 5.1e-3, comp = 6e-3, d = 1.5e-4, h = 1.38e-3, Kc = 1, sup;
	complex<double> teste1, teste2, teste3;
	char ch;
	string nome, substrato, arquivo1, arquivo2, arquivo3, lixo, gcheck, path, path2;
	t_lista_enc *l = new t_lista_enc;
	t_data novo;
	vector<double> field;


/*<=================================================================================================================>*/

	// Inicializa a lista encadeada
	inicializar(l);

/*<=================================================================================================================>*/

	// Do-While responsável por detectar problemas nos arquivos e perguntar novamente os nomes
	do
	{
		// Obter o nome dos três arquivos que serão trabalhados
		arquivo1 = argv[1];
		arquivo2 = argv[2];
		arquivo3 = argv[3];

		// Obter a pasta do arquivo da amostra
		path = argv[1];
		lixo = path.back();
		#ifdef _WIN32
		{
			while(lixo != "\\")
			{
				path.pop_back();
				lixo = path.back();
			}

			path = path + "Permeability_Results\\";
			path2 = path;					// path2 será usado pelo C++ para abrir arquivos

			// Diretório com os resultados
			system(("mkdir \"" + path2 + "\"").c_str());
		}
		#endif

		#ifdef linux
		{
			while(lixo != "/")
			{
				path.pop_back();
				lixo = path.back();
			}

			path = path + "Permeability_Results/";
			path2 = path;					// path2 será usado pelo C++ para abrir arquivos

			// Remover espaços para usar comandos do terminal "system()" no linux
			i = path.find(" ");
			if(i != std::string::npos)
			{
				while(path.find(" ", i) != std::string::npos)
				{
					path.insert(path.find(" ", i), "\\");
					i = path.find(" ", i)+2;
				}
			}

			// Diretório com os resultados
			system(("mkdir " + path).c_str());
		}
		#endif
	
/*<=================================================================================================================>*/

		// If responsável por detectar se os arquivos existem ou não
		if(access(arquivo1.c_str(), F_OK) != -1 && access(arquivo2.c_str(), F_OK) != -1 && access(arquivo3.c_str(), F_OK) != -1)
		{
			// Obtém o comprimento em milímetros da amostra e converte para metros
			lamostra = atof(argv[4]);
			lamostra *= 1e-3;

			// Obtém a expessura em milímetros e converte para metros
			d = atof(argv[5]);
			d *= 1e-9;

			// Obtém o fator de proporcionalidade estrutural
			Kc = 1;

			// Calcula o comprimento l_vazio
			double lvazio = comp - lamostra;

/*<=================================================================================================================>*/

			// Abre um dos arquivos para leitura
			ifstream arq1(arquivo1.c_str());
			i = 0;
			count = 0;

			// Descarta a primeira linha do arquivo -> o cabeçalho
			getline(arq1, lixo);

			// Laço responsável por ler o arquivo e detectar o número de frequências criando estruturas na lista
			while(arq1)
			{
				arq1 >> teste >> ch;		// Guarda a frequência em "teste". "ch" serve para descartar as vírgulas das colunas
				arq1 >> campo;

				// Salva o primeiro valor de campo em "teste2" para comparação
				if(flag == 0)
				{
					sup = campo;
					flag++;
				}

				// O arquivo é organizado por ordem de campo. Esse if detecta quando o campo muda e, portanto, todas frequencias foram salvas
				if(campo == sup)
				{
					// Função para criar a estrutura com a frequência como característica
					novo.freq = teste;
					inserir(l, novo);

					// Laço para descartar as outras colunas do arquivo
					for(j = 2; j < 5; j++)
					{
						arq1 >> ch;
						arq1 >> lixo2;
					}

					// "count" está contando o número de frequências
					count++;
					i++;
				}

				else
					break;
			}
			arq1.close();

			// "m" será a variável responsável por guardar o número de frequências com que estamos lidando
			m = count;

			// Abrimos o arquivo para contar o número de linhas
			arq1.open(arquivo1.c_str());

			count = 0;

			// Laço responsável por contar a quantidade de linhas no arquivo
			while(getline(arq1, lixo))
				count++;

			arq1.close();

			// Size será a variável responsável por guardar o número de valores de campo utilizados
			size = (count-1)/m;

			// Flag, a partir daqui, será a variável com o número de frequências
			flag = m;

/*<=================================================================================================================>*/
/*														     */
/*<=================================================================================================================>*/

			arq1.open(arquivo1.c_str());
			ifstream arq2(arquivo2.c_str());
			ifstream arq3(arquivo3.c_str());

			// Eliminar a primeira linha dos arquivos
			getline(arq1, lixo);
			getline(arq2, lixo);
			getline(arq3, lixo);

			// Resetar as variáveis
			i = 0;
			count = 0;
			sup = RAND_MAX;		// "lixo3" serve para detectarmos quando o valor de campo mudou

/*<=================================================================================================================>*/

			// Laço responsável por varrer os arquivos buscando os dados desejados
			while(arq1)
			{
				arq1 >> lixo2 >> ch;
				arq2 >> lixo2 >> ch;
				arq3 >> lixo2 >> ch;
				arq1 >> campo >> ch;
				arq2 >> lixo2 >> ch;
				arq3 >> lixo2 >> ch;

				// Condicional para guardar os valores de campo
				if(campo != sup)
				{
					field.push_back(campo);
					i++;
					sup = campo;
				}

				// Laço para obter partes reais e imaginarias dos arquivos e guardá-las em uma variável complexa
				arq1 >> real >> ch;
				arq1 >> imag >> ch;
				teste1 = complex<double> (real, imag);

				arq2 >> real >> ch;
				arq2 >> imag >> ch;
				teste2 = complex<double> (real, imag);

				arq3 >> real >> ch;
				arq3 >> imag >> ch;
				teste3 = complex<double> (real, imag);

				// Função para salvar os valores em seus respectivos vetores nas estruturas de dados
				alocar(l, teste1, teste2, teste3, count%flag, i-1);

				// Descartar as colunas que não serão utilizadas
				arq1 >> lixo2 >> ch;
				arq2 >> lixo2 >> ch;
				arq3 >> lixo2 >> ch;

				count++;
			}

			arq1.close();
			arq2.close();
			arq3.close();

/*<=================================================================================================================>*/

			// Criação de uma pasta onde haverão arquivos para cada valor de frequencia medida
			#ifdef _WIN32
			{
				system(("mkdir \"" + path2 + "Order_By_Freq\"").c_str());
			}
			#endif

			#ifdef linux
			{
				system(("mkdir "+ path + "Order_By_Freq").c_str());

			}
			#endif

			ofstream arq4;

			// Laço para fazer o print de dados por campo por frequencia
			for (j = 0; j < flag; j++)
			{
				// Condicional para printar um número limitado de frequências
				if(j%20 == 0)
				{
					// Arquivo com os dados para cada valor de frequencia individualmente
					#ifdef _WIN32
					{
						lixo = path2 + "Order_By_Freq\\" + std::to_string(j) + ".dat";
					}
					#endif

					#ifdef linux
					{
						lixo = path2 + "Order_By_Freq/" + std::to_string(j) + ".dat";
					}
					#endif
					
					arq4.open(lixo.c_str());
					arq4 << "Field\tFreq\tRe(Perm_eff)\tIm(Perm_eff)\tRe(Perm_r)\tIm(Perm_r)" << endl;	// Cabeçalho
					arq4 << std::fixed;									// Evitar notação científica
				}

				for (i = 0; i < size; i++)
				{
					// Função que fará os calculos e colocará os resultados em seus respectivos vetores
					analise(l, lvazio, lamostra, j, i, Kc, d, h);

					// Função para escrever os resultados em arquivos
					if(j%20 == 0)
						escrever2(l, j, arq4, field, i);
				}

				if(j%20 == 0)
					arq4.close();
			}

			arq4.close();

/*<=================================================================================================================>*/

			// O arquivo "Data.dat" contém todos os dados de campo, frequência e partes reais e imaginárias das
			// permeabilidades efetiva e relativa
			arq4.open((path2 + "Data.dat").c_str());
			arq4 << "Field\tFreq\tRe(Perm_eff)\tIm(Perm_eff)\tRe(Perm_r)\tIm(Perm_r)" << endl;	// Cabeçalho

			// Criação de uma pasta onde haverão arquivos para cada valor de campo medido
			#ifdef _WIN32
			{
				system(("mkdir \"" + path2 + "Order_By_Field\"").c_str());
			}
			#endif

			#ifdef linux
			{
				system(("mkdir " + path + "Order_By_Field").c_str());
			}
			#endif
			ofstream arq5;

			// Variável para dar nome ao arquivo com 1 casa decimal de campo
			std::stringstream stream;

			// Laço para fazer a análise de dados por frequência por campo
			for (i = 0; i < size; i++)
			{
				stream.str("");
				stream << std::fixed << std::setprecision(1) << field[i];	// Variável com o valor de campo com casa decimal correta

				// Arquivo com os mesmos dados do anterior, mas para cada valor de campo individualmente
				#ifdef _WIN32
				{
					lixo = path2 + "Order_By_Field\\H=" + stream.str() + ".dat";
				}
				#endif

				#ifdef linux
				{
					lixo = path2 + "Order_By_Field/H=" + stream.str() + ".dat";
				}
				#endif
				
				arq5.open(lixo.c_str());

				arq5 << "Freq\tRe(Perm_eff)\tIm(Perm_eff)\tRe(Perm_r)\tIm(Perm_r)" << endl;	// Cabeçalho
				arq5 << std::fixed;								// Evitar notação científica 

				for (j = 0; j < flag; j++)
				{
					// Função para escrever os resultados em ambos os arquivos
					escrever(l, j, arq5, arq4, field, i);
				}

				arq5.close();
			}
			arq4.close();

/*<=================================================================================================================>*/

			// Arquivo que conterá as larguras de linhas de ressonância
			arq4.open((path2 + "Linewidth.dat").c_str());
			arq4 << "Freq\tLinewidth(Oe-)\tLinewidth(Oe+)\tErrField(Oe)" << endl;

			m = 0;

			for(i = 0; i < flag; i++)
			{
				m++;

				// Funções responsáveis por encontrar o índice em que estão os mínimos e máximos da frequência (se existem)
				// Consertam problemas de descotinuidade da stripline
				min1 = min_negative_field2(l, i, field);
				if(min1 != RAND_MAX)
					max1 = max_negative_field2(l, i, field, min1);

				min2 = min_positive_field2(l, i, field, size);
				if(min2 != RAND_MAX)
					max2 = max_positive_field2(l, i, field, min2);

				if(max2 == RAND_MAX || max1 == RAND_MAX || min2 == RAND_MAX || min1 == RAND_MAX)
				{
					max1 = max_negative_field(l, i, field);
					if(max1 != RAND_MAX)
						min1 = min_negative_field(l, i, field, max1);

					max2 = max_positive_field(l, i, field, size);
					if(max2 != RAND_MAX)
						min2 = min_positive_field(l, i, field, max2);

					if(max2 == RAND_MAX || max1 == RAND_MAX || min2 == RAND_MAX || min1 == RAND_MAX)
						;

					else if(fabs(field[max1]-field[min1]) > 50 || fabs(field[max2]-field[min2]) > 50)
						;

					else
						// Função para imprimir as larguras
						print_min2(l, i, arq4, min1, min2, max1, max2, field);

				}

				else if(fabs(field[max1]-field[min1]) > 50 || fabs(field[max2]-field[min2]) > 50)
					;

				else
					// Função para imprimir as larguras
					print_min(l, i, arq4, min1, min2, max1, max2, field);

			}

			arq4.close();

/*<=================================================================================================================>*/

			// Arquivo que conterá a relação de dispersão de ressonância
			arq4.open((path2 + "Resonance.dat").c_str());
			arq4 << "Field\tFreq" << endl;

			m = 0;

			for(i = 0; i < flag; i++)
			{
				m++;

				// Funções responsáveis por encontrar o índice em que estão os mínimos e máximos da frequência (se existem)
				max1 = ress_negative_field(l, i, field, size);
				max2 = ress_positive_field(l, i, field, size);

				// Condicional para decidir se há ressonância
				if(max2 == RAND_MAX || max1 == RAND_MAX)
					;
				else
					// Função para imprimir a dispersão
					print_max(l, i, arq4, max1, max2, field);

			}

			arq4.close();

		}

	} while(access(arquivo1.c_str(), F_OK) == -1 || access(arquivo2.c_str(), F_OK) == -1 || access(arquivo3.c_str(), F_OK) == -1);

/*<=================================================================================================================>*/
/*														     */
/*<=================================================================================================================>*/

	// Liberar a memória e deletar a lista encadeada
	delete l;

	return 0;
}


/*<=================================================================================================================>*/
/*<=================================================================================================================>*/
/*<=================================================================================================================>*/


// Inicializar a lista
void inicializar(t_lista_enc *l)
{
	l->ini = NULL;
}

/*<=================================================================================================================>*/

// Insere novos objetos na lista
void inserir(t_lista_enc *l, t_data dado)
{
	t_nodo *novo = NULL;
	t_nodo *ant = NULL;
  	t_nodo *aux = l->ini;
  	novo = new t_nodo;

  	novo->data = dado;

  	while((aux != NULL))
  	{
    		ant = aux;
    		aux = aux->prox;
  	}

  	if(ant == NULL)
  	{
    		novo->prox = l->ini;
    		l->ini = novo;
  	}

  	else
  	{
    		novo->prox = ant->prox;
    		ant->prox = novo;
  	}
}

/*<=================================================================================================================>*/

// Função de salvar dados dos arquivos na lista em seus respectivos vetores
void alocar(t_lista_enc *l, const complex<double> &amostra, const complex<double> &substrato, const complex<double> &vazio, int j, int k)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < j; i++)
		aux = aux->prox;

	aux->data.amostra.push_back(amostra);
	aux->data.substrato.push_back(substrato);
	aux->data.vazio.push_back(vazio);
}

/*<=================================================================================================================>*/

// Função que calcula a permissividade da cavidade vazia
complex<double> permampty(t_lista_enc *l, double lvazio, double lamostra, int j, int i)
{
	t_nodo *aux = l->ini;
	complex<double> I(0,1);

	for(int k = 0; k < j; k++)
		aux = aux->prox;

	double w = 2.0*M_PI*aux->data.freq;
	complex<double> perm;

	perm = (I*c*log(-aux->data.vazio[i]))/(2.0*w*(lamostra+lvazio));

	return perm*perm;

}

/*<=================================================================================================================>*/

// Função que calcula a permissividade da cavidade com substrato
complex<double> permsub(t_lista_enc *l, double lvazio, double lamostra, int j, int i)
{
	t_nodo *aux = l->ini;
	complex<double> I(0,1);

	for(int k = 0; k < j; k++)
		aux = aux->prox;

	double w = 2.0*M_PI*aux->data.freq;
	complex<double> perm;
	complex<double> perm1 = aux->data.perm_emp[i];

	perm = (I*c*log(-aux->data.substrato[i]))/(2.0*w*lamostra);
	perm = perm - ((sqrt(perm1)*lvazio)/lamostra);

	return perm*perm;

}

/*<=================================================================================================================>*/

// Função que calcula a permeabilidade efetiva do filme
complex<double> pereff(t_lista_enc *l, double lvazio, double lamostra, int j, int i)
{
	t_nodo *aux = l->ini;
	complex<double> I(0,1);

	for(int k = 0; k < j; k++)
		aux = aux->prox;

	double w = 2.0*M_PI*aux->data.freq;
	complex<double> perm;
	complex<double> perm1 = aux->data.perm_emp[i];
	complex<double> perm2 = aux->data.perm_subs[i];

	perm = (I*c*log(-aux->data.amostra[i]))/(2.0*w*lamostra*sqrt(perm2));
	perm = perm - ((sqrt(perm1)*lvazio)/(sqrt(perm2)*lamostra));

	return perm*perm;
}

/*<=================================================================================================================>*/

// Função que escreve os resultados em dois arquivos diferentes: ordenado por campo e o espectro de permeabilidade completo
void escrever(t_lista_enc *l, int j, ofstream &arq1, ofstream &arq2, const vector<double> &field, int i)
{
	t_nodo *aux = l->ini;

	for(int k = 0; k < j; k++)
		aux = aux->prox;

	arq1 << aux->data.freq << "\t" << real(aux->data.ueff[i]) << imag(aux->data.ueff[i]) << "\t" <<  real(aux->data.ur[i]) << "\t" << imag(aux->data.ur[i]) << endl;
	arq2 << field[i] << "\t" << aux->data.freq << "\t" << real(aux->data.ueff[i]) << "\t" << imag(aux->data.ueff[i]) << "\t" << real(aux->data.ur[i]) << "\t" << imag(aux->data.ur[i]) << endl;
}

/*<=================================================================================================================>*/

// Escreve todo os dados de permeabilidade no arquivo ordenado por frequência
void escrever2(t_lista_enc *l, int j, ofstream &arq1, const vector<double> &field, int i)
{
	t_nodo *aux = l->ini;

	for(int k = 0; k < j; k++)
		aux = aux->prox;

	arq1 << field[i] << "\t" << aux->data.freq << "\t" << real(aux->data.ueff[i]) << "\t" << imag(aux->data.ueff[i]) << "\t" << real(aux->data.ur[i]) << "\t" << imag(aux->data.ur[i]) << endl;
}

/*<=================================================================================================================>*/

// Função que salva os resultados do modelo de V. Bekker et al. a vetores
void analise(t_lista_enc *l, double lvazio, double lamostra, int j, int i, double Kc, double d, double h)
{
	t_nodo *aux = l->ini;

	for(int k = 0; k < j; k++)
		aux = aux->prox;

	// perm1 é a permissividade para a cavidade vazia
	aux->data.perm_emp.push_back(permampty(l, lvazio, lamostra, j, i));

	// perm2 é a permissividade para a cavidade com o substrato
	aux->data.perm_subs.push_back(permsub(l, lvazio, lamostra, j, i));

	// ueff é a permeabilidade efetiva do filme
	aux->data.ueff.push_back(pereff(l, lvazio, lamostra, j, i));

	// ur é a permeabilidade relativa do filme
	aux->data.ur.push_back((aux->data.ueff[i]-1.0)/(Kc*(d/h)));
}

/*<=================================================================================================================>*/

// Obtém o máximo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com acmpo < 0
int max_negative_field(t_lista_enc *l, int k, const vector<double> &field)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, max = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(h = 1; field[h] < 0; h++)
	{
		d1 = (real(aux->data.ueff[h+1])-real(aux->data.ueff[h-1]))/(2.0*dh);

		// Condicional que detecta derivada positiva por se aproximar de um máximo
		if(d1 > 0)
			flag2++;
		else
			flag2 = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído de crescimento da função
		if(flag2 == 6)
		{
			// Laço para varrer os dados com campo negativo
			for(i = h-5; field[i] < 0; i++)
			{
				d2 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);

				// Condicional que detecta derivada negativa por passar de um máximo
				if(d2 < 0)
					flag++;
				else
					flag = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído do máximo
				if(flag == 6)
				{
					max = i-12;

					// Laço para achar o máximo
					for(j = i-12; j < i+30; j++)
						if(real(aux->data.ueff[j]) > real(aux->data.ueff[max]))
							max = j;
					break;
				}
			}
			break;
		}
	}
	return max;
}

/*<=================================================================================================================>*/

// Obtém o mínimo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com campo < 0
int min_negative_field(t_lista_enc *l, int k, const vector<double> &field, int max)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, flag2 = 0, min = RAND_MAX, teste = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(i = max; field[i] < 0; i++)
	{
		d1 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);

		// Condicional que detecta derivada positiva por passar de um mínimo
		if(d1 > 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído de mínimo
		if(flag == 5)
		{
			for(j = i-5; j < i+20; j++)
			{
				d2 = (real(aux->data.ueff[j+1])-real(aux->data.ueff[j-1]))/(2.0*dh);
				// Condicional que detecta derivada negativa por se aproxima de um decréscimo
				if(d2 < 0)
					flag2++;
				else
					flag2 = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag2 == 5)
				{
					teste = j-5;
					break;
				}
			}
			min = i-15;

			if(teste == RAND_MAX)
			{
				// Laço para achar o mínimo sem decréscimo pós mínimo
				for(j = i-15; j < i+20; j++)
					if(real(aux->data.ueff[j]) < real(aux->data.ueff[min]))
						min = j;
				break;
			}

			else
			{
				// Laço para achar o mínimo com decréscimo pós mínimo
				for(j = i-20; j < teste; j++)
					if(real(aux->data.ueff[j]) < real(aux->data.ueff[min]))
						min = j;
				break;
			}
		}
	}

	if(min != max && min > max)
		return min;
	else
		return RAND_MAX;
}

/*<=================================================================================================================>*/

// Obtém o máximo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com campo > 0
int max_positive_field(t_lista_enc *l, int k, const vector<double> &field, int size)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, max = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(h = size-2; field[h] > 0; h--)
	{
		d1 = (real(aux->data.ueff[h+1])-real(aux->data.ueff[h-1]))/(2.0*dh);

		// Condicional que detecta derivada negativa por se aproximar de um máximo
		if(d1 < 0)
			flag2++;
		else
			flag2 = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag2 == 6)
		{
			// Laço para varrer os dados com campo negativo
			for(i = h+5; field[i] > 0; i--)
			{
				d2 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);

				// Condicional que detecta derivada positiva por passar de um máximo
				if(d2 > 0)
					flag++;
				else
					flag = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag == 6)
				{
					max = i+12;

					// Laço para achar o máximo
					for(j = i+12; j > i-30; j--)
						if(real(aux->data.ueff[j]) > real(aux->data.ueff[max]))
							max = j;
					break;
				}
			}
			break;
		}
	}
	return max;
}

/*<=================================================================================================================>*/

// Obtém o mínimo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com campo > 0
int min_positive_field(t_lista_enc *l, int k, const vector<double> &field, int max)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, flag2 = 0, min = RAND_MAX, teste = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(i = max; field[i] > 0; i--)
	{
		d1 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);
		// Condicional que detecta derivada negativa por passar de um mínimo
		if(d1 < 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 5)
		{
			for(j = i+3; j > i-20; j--)
			{
				d2 = (real(aux->data.ueff[j+1])-real(aux->data.ueff[j-1]))/(2.0*dh);

				// Condicional que detecta derivada positiva por se aproximar de um decréscimo
				if(d2 > 0)
					flag2++;
				else
					flag2 = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag2 == 5)
				{
					teste = j+5;
					break;
				}
			}
			min = i+20;

			if(teste == RAND_MAX)
			{
				// Laço para achar o mínimo sem decréscimo pós mínimo
				for(j = i+20; j > i-20; j--)
					if(real(aux->data.ueff[j]) < real(aux->data.ueff[min]))
						min = j;
				break;
			}

			else
			{
				// Laço para achar o mínimo com decréscimo pós mínimo
				for(j = i+20; j > teste; j--)
					if(real(aux->data.ueff[j]) < real(aux->data.ueff[min]))
						min = j;
				break;
			}
		}
	}

	if(min != max && min < max)
		return min;
	else
		return RAND_MAX;
}

/*<=================================================================================================================>*/

// Obtém o mínimo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com campo < 0
int min_negative_field2(t_lista_enc *l, int k, const vector<double> &field)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, min = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(h = 1; field[h] < 0; h++)
	{
		d1 = (real(aux->data.ueff[h+1])-real(aux->data.ueff[h-1]))/(2.0*dh);

		// Condicional que detecta derivada positiva por se aproximar de um mínimo
		if(d1 < 0)
			flag2++;
		else
			flag2 = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído de decrescimento da função
		if(flag2 == 6)
		{
			// Laço para varrer os dados com campo negativo
			for(i = h-5; field[i] < 0; i++)
			{
				d2 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);

				// Condicional que detecta derivada positiva por passar de um mínimo
				if(d2 > 0)
					flag++;
				else
					flag = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído do mínimo
				if(flag == 6)
				{
					min = i-12;

					// Laço para achar o mínimo
					for(j = i-20; j < i+30; j++)
						if(real(aux->data.ueff[j]) < real(aux->data.ueff[min]))
							min = j;
					break;
				}
			}
			break;
		}
	}
	return min;
}

/*<=================================================================================================================>*/

// Obtém o máximo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com campo < 0
int max_negative_field2(t_lista_enc *l, int k, const vector<double> &field, int min)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, flag2 = 0, max = RAND_MAX, teste = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(i = min; field[i] < 0; i++)
	{
		d1 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);

		// Condicional que detecta derivada negativa por passar de um máximo
		if(d1 < 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído de máximo
		if(flag == 5)
		{
			for(j = i-5; j < i+20; j++)
			{
				d2 = (real(aux->data.ueff[j+1])-real(aux->data.ueff[j-1]))/(2.0*dh);
				// Condicional que detecta derivada negativa por se aproxima de um decréscimo

				if(d2 > 0)
					flag2++;
				else
					flag2 = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag2 == 5)
				{
					teste = j-5;
					break;
				}
			}
			max = i-20;

			if(teste == RAND_MAX)
			{
				// Laço para achar o mínimo sem crescimento pós máximo
				for(j = i-20; j < i+20; j++)
					if(real(aux->data.ueff[j]) > real(aux->data.ueff[max]))
						max = j;
				break;
			}

			else
			{
				// Laço para achar o máximo com crescimento pós máximo
				for(j = i-20; j < teste; j++)
					if(real(aux->data.ueff[j]) > real(aux->data.ueff[max]))
						max = j;
				break;
			}
		}
	}

	if(min != max && min < max)
		return max;
	else
		return RAND_MAX;
}

/*<=================================================================================================================>*/

// Obtém o mínimo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com campo > 0
int min_positive_field2(t_lista_enc *l, int k, const vector<double> &field, int size)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, h, flag = 0, flag2 = 0, min = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(h = size-2; field[h] > 0; h--)
	{
		d1 = (real(aux->data.ueff[h+1])-real(aux->data.ueff[h-1]))/(2.0*dh);

		// Condicional que detecta derivada positiva por se aproximar de um mínimo
		if(d1 > 0)
			flag2++;
		else
			flag2 = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag2 == 6)
		{
			// Laço para varrer os dados com campo positivo
			for(i = h+5; field[i] > 0; i--)
			{
				d2 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);

				// Condicional que detecta derivada negativa por passar de um mínimo
				if(d2 < 0)
					flag++;
				else
					flag = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag == 6)
				{
					min = i+12;

					// Laço para achar o máximo
					for(j = i+20; j > i-30; j--)
						if(real(aux->data.ueff[j]) < real(aux->data.ueff[min]))
							min = j;
					break;
				}
			}
			break;
		}
	}
	return min;
}

/*<=================================================================================================================>*/

// Obtém o máximo na parte real da permeab. para obter a largura de linha consertando problemas de descontinuidade com campo > 0
int max_positive_field2(t_lista_enc *l, int k, const vector<double> &field, int min)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, flag2 = 0, max = RAND_MAX, teste = RAND_MAX;
	double dh = field[2]-field[1], d1, d2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	for(i = min; field[i] > 0; i--)
	{
		d1 = (real(aux->data.ueff[i+1])-real(aux->data.ueff[i-1]))/(2.0*dh);

		// Condicional que detecta derivada positiva por passar de um máximo
		if(d1 > 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 5)
		{
			for(j = i+3; j > i-20; j--)
			{
				d2 = (real(aux->data.ueff[j+1])-real(aux->data.ueff[j-1]))/(2.0*dh);

				// Condicional que detecta derivada negativa por se aproximar de um crescimento
				if(d2 < 0)
					flag2++;
				else
					flag2 = 0;

				// Condicional que detecta se os dados passaram pelo critério de ruído
				if(flag2 == 5)
				{
					teste = j+5;
					break;
				}
			}
			max = i+20;

			if(teste == RAND_MAX)
			{
				// Laço para achar o mínimo sem crescimento pós máximo
				for(j = i+30; j > i-20; j--)
					if(real(aux->data.ueff[j]) > real(aux->data.ueff[max]))
						max = j;
				break;
			}

			else
			{
				// Laço para achar o mínimo com crescimento pós máximo
				for(j = i+20; j > teste; j--)
					if(real(aux->data.ueff[j]) > real(aux->data.ueff[max]))
						max = j;
				break;
			}
		}
	}

	if(min != max && min > max)
		return max;
	else
		return RAND_MAX;
}

/*<=================================================================================================================>*/

// Função que escreve os valores de largura de linha no arquivo corrigindo problemas de descontinuidade
void print_min(t_lista_enc *l, int k, ofstream &arq1, int min1, int min2, int max1, int max2, const vector<double> &field)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < k; i++)
		aux = aux->prox;

	arq1 << aux->data.freq << "\t" << field[max1]-field[min1] << "\t" << field[min2]-field[max2] << "\t" << "1.0606601717798212866" << endl;
}

/*<=================================================================================================================>*/

// Função que escreve os valores de largura de linha no arquivo corrigindo problemas de descontinuidade
void print_min2(t_lista_enc *l, int k, ofstream &arq1, int min1, int min2, int max1, int max2, const vector<double> &field)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < k; i++)
		aux = aux->prox;

	arq1 << aux->data.freq << "\t" << field[min1]-field[max1] << "\t" << field[max2]-field[min2] << "\t" << "1.0606601717798212866" << endl;
}

/*<=================================================================================================================>*/

// Função que obtém o valor em campo de ressonância para uma dada frequência quando o campo é negativo
int ress_negative_field(t_lista_enc *l, int k, const vector<double> &field, int size)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, ress1 = RAND_MAX;
	double d, dh, signal = 0.0;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	dh = 2*(field[2]-field[1]);

	for (i = 0; field[i] < 0; i++)
	{
		if (fabs(imag(aux->data.ueff[i])) > fabs(signal))
			signal = imag(aux->data.ueff[i]);
	}

	// Condicional que tentar arrumar erros causados pela descontinuidade da linestrip
	if(signal < 0)
	{
		// Laço para varrer os dados com campo negativo
		for(i = 1; field[i] < 0; i++)
		{
			d = (imag(aux->data.ueff[i+1])-imag(aux->data.ueff[i-1]))/dh;

			// Condicional que detecta derivada negativa
			if(d < 0)
				flag++;
			else
				flag = 0;

			// Condicional que detecta se os dados passaram pelo critério de ruído
			if(flag == 7)
			{
				ress1 = i-6;

				// Laço para achar o mínimo
				for(j = i-6; field[j] < 0; j++)
					if(imag(aux->data.ueff[j]) < imag(aux->data.ueff[ress1]))
						ress1 = j;
				break;
			}
		}
	}

	else
	{
		// Laço para varrer os dados com campo negativo
		for(i = 1; field[i] < 0; i++)
		{
			d = (imag(aux->data.ueff[i+1])-imag(aux->data.ueff[i-1]))/dh;

			// Condicional que detecta derivada positiva
			if(d > 0)
				flag++;
			else
				flag = 0;

			// Condicional que detecta se os dados passaram pelo critério de ruído
			if(flag == 7)
			{
				ress1 = i-6;

				// Laço para achar o máximo
				for(j = i-6; field[j] < 0; j++)
					if(imag(aux->data.ueff[j]) > imag(aux->data.ueff[ress1]))
						ress1 = j;
				break;
			}
		}
	}

	return ress1;
}

/*<=================================================================================================================>*/

// Função que obtém o valor em campo de ressonância para uma dada frequência quando o campo é positivo
int ress_positive_field(t_lista_enc *l, int k, const vector<double> &field, int size)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, ress2 = RAND_MAX;
	double d, dh, signal = 0;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	dh = 2*(field[2]-field[1]);

	for (i = 0; field[i] < 0; i++)
	{
		if (fabs(imag(aux->data.ueff[i])) > fabs(signal))
			signal = imag(aux->data.ueff[i]);
	}

	// Condicional que tentar arrumar erros causados pela descontinuidade da linestrip
	if(signal < 0)
	{
		// Laço para varrer os dados com campo positivo
		for(i = size-2; field[i] >= 0; i--)
		{
			d = (imag(aux->data.ueff[i+1])-imag(aux->data.ueff[i-1]))/dh;

			// Condicional que detecta derivada positiva
			if(d > 0)
			{
				flag++;
			}
			else
				flag = 0;

			// Condicional que detecta se os dados passaram pelo critério de ruído
			if(flag == 7)
			{
				ress2 = i+6;

				// Laço para achar o mínimo
				for(j = i+6; field[j] >= 0; j--)
					if(imag(aux->data.ueff[j]) < imag(aux->data.ueff[ress2]))
						ress2 = j;
				break;
			}
		}
	}

	else
	{
		// Laço para varrer os dados com campo positivo
		for(i = size-2; field[i] >= 0; i--)
		{
			d = (imag(aux->data.ueff[i+1])-imag(aux->data.ueff[i-1]))/dh;

			// Condicional que detecta derivada negativa
			if(d < 0)
			{
				flag++;
			}
			else
				flag = 0;

			// Condicional que detecta se os dados passaram pelo critério de ruído
			if(flag == 7)
			{
				ress2 = i+6;

				// Laço para achar o máximo
				for(j = i+6; field[j] >= 0; j--)
					if(imag(aux->data.ueff[j]) > imag(aux->data.ueff[ress2]))
						ress2 = j;
				break;
			}
		}
	}

		return ress2;
}

/*<=================================================================================================================>*/

// Salva a relação de dispersão de ressonância no arquivo 
void print_max(t_lista_enc *l, int k, ofstream &arq, int ress1, int ress2, const vector<double> &field)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < k; i++)
		aux = aux->prox;

	arq << field[ress1] << "\t" << aux->data.freq << endl;
	arq << field[ress2] << "\t" << aux->data.freq << endl;

}
