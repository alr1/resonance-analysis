#include <iostream>
#include <cstdlib>
#include <cmath>
#include <unistd.h>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using std::endl;
using std::vector; 		using std::string;
using std::ofstream; 	using std::ifstream;


// Estrutura responsável por guardar os arquivos em formato lista encadeada
struct t_data 							
{
	double freq;
	vector<double> s11;
	vector<double> s21;
	vector<double> s11Cal;
	vector<double> s21Cal;
};

// Estrutura de encadeamento -> nodo
struct t_nodo
{
	t_data data;
	struct t_nodo *prox;
};

// Estrutura de lista que aponta para uma das estruturas de dados
struct t_lista_enc
{
	t_nodo *ini;
};

void inicializar(t_lista_enc *);
void inserir(t_lista_enc *, t_data);
void alocar(t_lista_enc *, double, double, double, double, int, int);
double func(double, double);
int min_negative_field(t_lista_enc *, int, const vector<double> &);
int min_positive_field(t_lista_enc *, int, const vector<double> &, int);
double cal(t_nodo *, int);
void print_min(t_lista_enc *, int, ofstream &, int, int, const vector<double> &);
void print_freq(t_lista_enc *, int, int, const vector<double> &, string&, string&);

int main(int argc, char** argv)
{
	int i, j, k, m, size, count = 0, flag = 0, min1, min2;
	double campo, teste, teste2, teste3, teste4, lixo2, lixo3;
	char ch;
	string nome, arquivo1, arquivo2, arquivo3, arquivo4, lixo, holder, gcheck, path, path2;
	t_lista_enc *l = new t_lista_enc;
	t_data novo;
	vector<double> field;


/*<=================================================================================================================>*/
	
	// Inicializa a lista encadeada
	inicializar(l);

/*<=================================================================================================================>*/

	// Do-While responsável por tentar abrir os arquivos
	do
	{

		// Obter os nomes, passados ao programa, dos quatro arquivos que serão utilizados
		// Arquivos da amostra
		arquivo1 = argv[1];
		arquivo2 = argv[2];

		// Arquivos de calibração
		arquivo3 = argv[3];
		arquivo4 = argv[4];

		// Obter a pasta dos arquivos da amostra -> onde os resultados serão salvos
		path = argv[1];
		lixo = path.back();

		#ifdef _WIN32
		{
			while(lixo != "\\")
			{
				path.pop_back();
				lixo = path.back();
			}

			path = path + "Absorption_Results\\";
			path2 = path;				// path2 será usado pelo C++ para abrir arquivos
			
			// Diretório com os resultados
			system(("mkdir \"" + path2 + "\"").c_str());

		}
		#endif

		#ifdef linux
		{
			while(lixo != "/")
			{
		 		path.pop_back();
		 		lixo = path.back();
			}

			path = path + "Absorption_Results/";
			path2 = path;				// path2 será usado pelo C++ para abrir arquivos
		
			// Remover espaços para usar comandos do terminal "system()" no linux
			i = path.find(" ");
			if(i != std::string::npos)
			{
				while(path.find(" ", i) != std::string::npos)
				{
					path.insert(path.find(" ", i), "\\");
					i = path.find(" ", i)+2;
				}
			}

			// Diretório com os resultados
			system(("mkdir " + path).c_str());

		}
		#endif

/*<=================================================================================================================>*/

		// If responsável por detectar se os arquivos existem ou não
		if(access(arquivo1.c_str(), F_OK) != -1 && access(arquivo2.c_str(), F_OK) != -1 && access(arquivo3.c_str(), F_OK) != -1 && access(arquivo4.c_str(), F_OK) != -1)
		{
			// Abre um dos arquivos para leitura
			ifstream arq1(arquivo1.c_str());
			i = 0;
			count = 0;

			// Descarta a primeira linha do arquivo -> o cabeçalho
			getline(arq1, lixo);

/*<=================================================================================================================>*/

			// Laço responsável por ler o arquivo e detectar o número de frequências criando estruturas na lista
			while(arq1)
			{
				arq1 >> teste;	arq1 >> ch;		// Guarda a frequência em "teste". "ch" serve para descartar as vírgulas das colunas
				arq1 >> campo;

				// Salva o primeiro valor de campo em "teste2" para comparação
				if(flag == 0)
				{
					teste2 = campo;
					flag++;
				}

				// O arquivo é organizado por ordem de campo. Esse if detecta quando o campo muda e, portanto, todas frequências foram salvas
				if(campo == teste2)
				{
					// Função para criar a estrutura com a frequência como característica
					novo.freq = teste;
					inserir(l, novo);

					// Laço para descartar as outras colunas do arquivo
					for(j = 2; j < 5; j++)
					{
						arq1 >> ch;
						arq1 >> lixo2;
					}

					// "count" está contando o número de frequências
					count++;
					i++;
				}

				else
					break;
			}
			arq1.close();

/*<=================================================================================================================>*/

			// "m" será a variável responsável por guardar o número de frequências com que estamos lidando
			m = count;

			// Abrimos o arquivo para contar o número de linhas
			arq1.open(arquivo1.c_str());

			count = 0;

			// Laço responsável por contar a quantidade de linhas no arquivo
			while(getline(arq1, lixo))
				count++;

			arq1.close();

			// Size será a variável responsável por guardar o número de valores de campo utilizados
			size = (count-1)/m;

			// Flag a partir daqui será a variável com o número de frequências
			flag = m;

/*<=================================================================================================================>*/
/*                                                                                                                   */
/*<=================================================================================================================>*/

			// Abrimos dessa vez todos os arquivos para salvarmos os valores de S11 e S21
			arq1.open(arquivo1.c_str());
			ifstream arq2(arquivo2.c_str());
			ifstream arq1Cal(arquivo3.c_str());
			ifstream arq2Cal(arquivo4.c_str());

			// Eliminar a primeira linha dos arquivos
			getline(arq1, lixo);
			getline(arq2, lixo);
			getline(arq1Cal, lixo);
			getline(arq2Cal, lixo);

			// Resetar as variáveis
			i = 0;
			count = 0;
			lixo3 = RAND_MAX;		// "lixo3" serve para detectarmos quando o valor de campo mudou

/*<=================================================================================================================>*/

			// Laço responsável por varrer os arquivos buscando os dados
			while(arq1)
			{
				arq1 >> lixo2;	arq1 >> ch;
				arq1 >> campo;	arq1 >> ch;

				arq2 >> lixo2; arq2 >> ch;
				arq2 >> lixo2;	arq2 >> ch;

				arq1Cal >> lixo2;	arq1Cal >> ch;
				arq1Cal >> campo;	arq1Cal >> ch;

				arq2Cal >> lixo2; arq2Cal >> ch;
				arq2Cal >> lixo2;	arq2Cal >> ch;

				// Condicional para guardar os valores de campo
				if(campo != lixo3)
				{
					field.push_back(campo);
					i++;
					lixo3 = campo;
				}

				// Laço para descartar as colunas que não serão utilizadas
				for(j = 2; j < 4; j++)
				{
						arq1 >> lixo2;	arq1 >> ch;
						arq2 >> lixo2; arq2 >> ch;

						arq1Cal >> lixo2;	arq1Cal >> ch;
						arq2Cal >> lixo2; arq2Cal >> ch;
				}

				// Salvamos os módulos de S11 e S21 nas variáveis "teste" e "teste2" para serem passadas para a função "alocar"
				arq1 >> teste;
				arq2 >> teste2;
				arq1Cal >> teste3;
				arq2Cal >> teste4;

				// Função para salvar os valores em seus respectivos vetores nas estruturas de dados
				alocar(l, teste, teste2, teste3, teste4, count%flag, i-1);

				count++;

			}

			arq1.close();
			arq2.close();
			arq1Cal.close();
			arq2Cal.close();

/*<=================================================================================================================>*/

			// Arquivo em que será salvo os dados para o gráfico de ressonância
			ofstream arq4((path2 + "FxH.dat").c_str());

			arq4 << "Field\tFreq" << endl;	// Cabeçalho
			arq4 << std::fixed;				// Comando para evitar o C++ de escrever em notação científica

			// Laço para varrer cada uma das frequências
			for(i = 0; i < flag; i++)
			{
				// Funções responsáveis por encontrar o índice em que estão os mínimos da frequência (se existem)
				min1 = min_negative_field(l, i, field);
				min2 = min_positive_field(l, i, field, size);

				// Condicional para decidir se há mínimo
				if(min2 == RAND_MAX || min1 == RAND_MAX)
					// Pular nos resultados que não há mínimo
					;
				else
					// Função para imprimir onde estão os mínimos e escrever no arquivo para fazer o gráfico
					print_min(l, i, arq4, min1, min2, field);
			}

			arq4.close();
		}
	} while(access(arquivo1.c_str(), F_OK) == -1 || access(arquivo2.c_str(), F_OK) == -1 || access(arquivo3.c_str(), F_OK) == -1 || access(arquivo4.c_str(), F_OK) == -1);

/*<=================================================================================================================>*/
/*														   */
/*<=================================================================================================================>*/


	// Função para printar S11^2+S21^2-1 em função do campo para diferentes frequências
	print_freq(l, flag, size, field, path2, path);

	// Liberar a memória e deletar a lista encadeada
	delete l;

/*<=================================================================================================================>*/

	return 0;
}

/*<=================================================================================================================>*/
/*<=================================================================================================================>*/
/*<=================================================================================================================>*/


// Inicializar a lista
void inicializar(t_lista_enc *l)
{
	l->ini = NULL;
}

/*<=================================================================================================================>*/

// Função para inserir novas estruturas na lista encadeada
void inserir(t_lista_enc *l, t_data dado)
{
	t_nodo *novo = NULL;
	t_nodo *ant = NULL;
	t_nodo *aux = l->ini;
	novo = new t_nodo;

	novo->data = dado;

	while((aux != NULL))
	{
		ant = aux;
		aux = aux->prox;
	}

	if(ant == NULL)
	{
		novo->prox = l->ini;
		l->ini = novo;
 	}

	else
	{
		novo->prox = ant->prox;
		ant->prox = novo;
	}
}

/*<=================================================================================================================>*/

// Função para salvar os dados dos arquivos em vetores
void alocar(t_lista_enc *l, double s11, double s21, double s11Cal, double s21Cal, int j, int k)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < j; i++)
		aux = aux->prox;

	aux->data.s11.push_back(s11);
	aux->data.s21.push_back(s21);
	aux->data.s11Cal.push_back(s11Cal);
	aux->data.s21Cal.push_back(s21Cal);
}

/*<=================================================================================================================>*/

// Função para obter as curvas de absorção S11²+S21²-1
double func(double S11, double S21)
{
	return (S11*S11) + (S21*S21) - 1;
}

/*<=================================================================================================================>*/

double cal(t_nodo *aux, int i)
{
	return func(aux->data.s11[i], aux->data.s21[i]) - func(aux->data.s11Cal[i], aux->data.s21Cal[i]);
}

/*<=================================================================================================================>*/

// Função para encontrar o mínimo para campos negativos
int min_negative_field(t_lista_enc *l, int k, const vector<double> &field)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, min1 = RAND_MAX;
	double d, dh;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	dh = 2*(field[2]-field[1]);

	// Laço para varrer os dados com campo negativo
	for(i = 1; field[i] < 0; i++)
	{
		d = (cal(aux, i+1)-cal(aux, i-1))/dh;

		// Condicional que detecta uma diminuição no sinal
		if(d < 0)
			flag++;
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 7)
		{
			min1 = i-6;

			// Laço para achar o mínimo
			for(j = i-6; field[j] < 0; j++)
				if(cal(aux, j) < cal(aux, min1))
					min1 = j;
			break;
		}
	}

	return min1;
}

/*<=================================================================================================================>*/

// Função para encontrar o mínimo para campos positivos
int min_positive_field(t_lista_enc *l, int k, const vector<double> &field, int size)
{
	// Flag será uma variável responsável por ser um critério para ignorar ruído
	int i, j, flag = 0, min2 = RAND_MAX;
	double d, dh, var1, var2;
	t_nodo *aux = l->ini;

	for(i = 0; i < k; i++)
		aux = aux->prox;

	dh = 2*(field[2]-field[1]);

	// Laço para varrer os dados com campo positivo
	for(i = size-2; field[i] >= 0; i--)
	{
		d = (cal(aux, i+1)-cal(aux, i-1))/dh;

		// Condicional que detecta uma diminuição no sinal
		if(d > 0)
		{
			flag++;
		}
		else
			flag = 0;

		// Condicional que detecta se os dados passaram pelo critério de ruído
		if(flag == 7)
		{
			min2 = i+6;

			// Laço para achar o mínimo
			for(j = i+6; field[j] >= 0; j--)
				if(cal(aux, j) < cal(aux, min2))
					min2 = j;
			break;
		}
	}

	// Condicional de filtragem
	if(min2 == RAND_MAX)
		return min2;
	else if(field[min2] < 2)
		return RAND_MAX;
	else
		return min2;
}

/*<=================================================================================================================>*/

// Função para printar no arquivo extra os minimos encontrados
void print_min(t_lista_enc *l, int k, ofstream &arq, int min1, int min2, const vector<double> &field)
{
	t_nodo *aux = l->ini;

	for(int i = 0; i < k; i++)
		aux = aux->prox;

	arq << field[min1] << "\t" << aux->data.freq << endl;
	arq << field[min2] << "\t" << aux->data.freq << endl;
}

/*<=================================================================================================================>*/

// Função para printar as curvas de absorção para cada frequência
void print_freq(t_lista_enc *l, int flag, int size, const vector<double> &field, string &path2, string &path)
{
	int i, j, k;
	string fr;
	ofstream arq;
	t_nodo *aux = l->ini;

	// Criar pasta para quardar os arquivos
	#ifdef _WIN32
	{
		system(("mkdir \"" + path2 + "Frequencies\"").c_str());
	}
	#endif

	#ifdef linux
	{
		system(("mkdir " + path + "Frequencies").c_str());
	}
	#endif

	// Laço para trabalhar diferentes frequências. 50 frequências serão printadas
	for(i = 0; i < flag; i += 20)
	{
		// Nome do arquivo
		#ifdef _WIN32
		{
			fr = path2 + "Frequencies\\" + std::to_string(i) + ".dat";
		}
		#endif

		#ifdef linux
		{
			fr = path2 + "Frequencies/" + std::to_string(i) + ".dat";
		}
		#endif

		arq.open(fr.c_str());
		arq << "Field\tFreq\t1-S11^2-S21^2" << endl;	// Cambeçalho
		arq << std::fixed;				// Evitar notação científica

		// Laço para printar as curvas de absorção em função do campo para cada frequência
		for(j = 0; j < size; j++)
			arq << field[j] << "\t" << aux->data.freq << "\t" << -cal(aux, j) << endl;

		arq.close();

		for(k = 0; k < 20; k++)
			aux = aux->prox;
	}
}
