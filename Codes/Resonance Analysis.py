#!/usr/bin/env python3
# -*- coding: utf-8 -*-

##<===================================================================================================>##

# Disclaimer: This is my first time trying to build a GUI. And that is why the code is a mess.
# Eventually, when I become more experienced with graphical interfaces, I may try to improve the code,
# but for now I am happy with it just working the way I expect it to.

##<===================================================================================================>##

import subprocess as sub
import tkinter as tk
from tkinter import ttk
from tkinter import filedialog as fd
from tkinter import messagebox
from tkinter.constants import CENTER
import sys
from threading import Thread
import os, platform

# Function to center the windows on the screena -----------------------------------------
def center(win):
    """
    centers a tkinter window
    :param win: the main window or Toplevel window to center
    """
    win.update_idletasks()
    width = win.winfo_width()
    frm_width = win.winfo_rootx() - win.winfo_x()
    win_width = width + 2 * frm_width
    height = win.winfo_height()
    titlebar_height = win.winfo_rooty() - win.winfo_y()
    win_height = height + titlebar_height + frm_width
    x = win.winfo_screenwidth() // 2 - win_width // 2
    y = win.winfo_screenheight() // 2 - win_height // 2
    win.geometry('{}x{}+{}+{}'.format(width, height, x, y))
    win.deiconify()

# Get the path of the python script to be able to find the c++ binaries location --------
if getattr(sys, 'frozen', False):
    path = sys._MEIPASS
else:
    path = os.path.dirname(os.path.abspath(__file__))
    path = str(path)

# If the OS is a linux system we need to insert the escape sequence '\' before blank spaces
if platform.system() == "Linux":
    path = path + "/"
    path = path.replace(" ", "\ ")

# Window settings
root = tk.Tk()
root.title('Choosing the method:')
root.resizable(False, False)
root.geometry('300x300')
root.grid_rowconfigure((0,5), weight=1)
root.grid_columnconfigure(0, weight=1)
center(root)

# Variable storing the chosen method ----------------------------------------------------
choice = tk.IntVar()

# Function that activates the "continue" button when a method is chosen -----------------
def myfunction(*event):
    if choice.get() == 1 or choice.get() == 2 or choice.get() == 3 or choice.get() == 4:
        exit_button.config(state='normal')
    else:
        exit_button.config(state='disabled')

# Methods -------------------------------------------------------------------------------
options = [("Complex Permeability", 1),
   	         ("Energy Absorption", 2),
    	     ("S11 Variation", 3),
             ("Magnetoimpedance", 4)]

# Function to get the chosen method value -----------------------------------------------
def getChoice():
    choice.get()

tk.Label(root, 
         text="""Choose the analysis method:""").grid()

# Display the mathods as radiobuttons ---------------------------------------------------
for option, val in options:
     tk.Radiobutton(root, 
                   text=option,
                   indicatoron = 0,
                   width = 20, 
                   variable=choice,
                   command=getChoice,
                   value=val).grid(pady=5)

# Continue button settings --------------------------------------------------------------
exit_button = ttk.Button(root, text="Continue", command=root.destroy)
exit_button.grid()
exit_button.config(state='disabled')
choice.trace('w', myfunction)

# Stop all process if windows is closed -------------------------------------------------
def on_closing():
    if messagebox.askyesno("Quit", "Do you want to quit?"):
        sys.exit()
root.protocol("WM_DELETE_WINDOW", on_closing)

# Run the window ------------------------------------------------------------------------
root.mainloop()

##<===================================================================================================>##
##                                        Method: Permeability                                         ##
##<===================================================================================================>##

if choice.get() == 1:

    # Create the root window ------------------------------------------------------------
    root = tk.Tk()
    root.title('Complex Permeability method:')
    root.resizable(False, False)
    root.geometry('800x400')
    center(root)
    root.grid_rowconfigure((0,6), weight=1)
    root.grid_columnconfigure((0,1,2,3,4,5), weight=1)

    # Function that activates the "continue" button -------------------------------------
    def myfunction(*event):
        if len(ent1.get()) > 1 and len(ent2.get()) > 1 and len(ent3.get()) > 1 and len(ent4.get()) > 0 and len(ent5.get()) > 0:
            exit_button.config(state='normal')
        else:
            exit_button.config(state='disabled')

    # Introduction ----------------------------------------------------------------------
    label = ttk.Label(root, text = "For this method 3 files are necessary: S11 of the empty stripline, "
                                    "S11 of the stripline loaded only with the substrate and S11 of the sample. "
                                    "You will also have to enter the sample's lenght and thickness.\n"
                                    "\nChoose the files to be analyzed:", 
                                    justify=CENTER, 
                                    wraplength=700)
    label.grid(row = 0, column = 0, columnspan=6)

    # Entry variables
    var = tk.StringVar(root); var2 = tk.StringVar(root); var3 = tk.StringVar(root)
    var.trace('w', myfunction); var2.trace('w', myfunction); var3.trace('w', myfunction)

    # File location dialogs -------------------------------------------------------------
    # ent1 -> Sample
    ent1=tk.Entry(root, justify=CENTER, textvariable=var, state=tk.DISABLED)
    ent1.grid(row = 1, column = 0, ipadx=140, columnspan=3)
    ent1.config(disabledforeground="black",disabledbackground="white")
    
    # ent2 -> Substrate
    ent2=tk.Entry(root, justify=CENTER, textvariable=var2, state=tk.DISABLED)
    ent2.grid(row = 2, column = 0, ipadx=140, columnspan=3)
    ent2.config(disabledforeground="black",disabledbackground="white")
    
    # ent3 -> Stripline
    ent3=tk.Entry(root, justify=CENTER, textvariable=var3, state=tk.DISABLED)
    ent3.grid(row = 3, column = 0, ipadx=140, columnspan=3)
    ent3.config(disabledforeground="black",disabledbackground="white")


    # Button definitions ----------------------------------------------------------------
    def select_fileSample():
        global filename1
        filename1 = fd.askopenfilename(title='Choose sample file')
        ent1.config(state=tk.NORMAL)
        ent1.delete(0, tk.END)
        ent1.insert(tk.END, filename1)
        ent1.config(state=tk.DISABLED)

    def select_fileSubstrate():
        global filename2
        filename2 = fd.askopenfilename(title='Choose substrate file')
        ent2.config(state=tk.NORMAL)
        ent2.delete(0, tk.END)
        ent2.insert(tk.END, filename2)
        ent2.config(state=tk.DISABLED)

    def select_fileSample_Holder():
        global filename3
        filename3 = fd.askopenfilename(title='Choose stripline file')
        ent3.config(state=tk.NORMAL)
        ent3.delete(0, tk.END)
        ent3.insert(tk.END, filename3)
        ent3.config(state=tk.DISABLED)

    # Open file buttons -----------------------------------------------------------------
    Samplefile = ttk.Button(
        root,
        text="Choose the sample file",
        command=select_fileSample,
        width = 27
    )
    Samplefile.grid(row = 1, column = 3, pady=10, columnspan=3)

    Substratefile = ttk.Button(
        root,
        text='Choose the substrate file',
        command=select_fileSubstrate,
        width = 27
    )
    Substratefile.grid(row = 2, column = 3, pady=10, columnspan=3)

    Holderfile = ttk.Button(
        root,
        text='Choose the empty stripline file',
        command=select_fileSample_Holder,
        width = 27
    )
    Holderfile.grid(row = 3, column = 3, pady=10, columnspan=3)

    # Sample parameters: ----------------------------------------------------------------
    varsample1 = tk.StringVar();           varsample2 = tk.StringVar()
    varsample1.trace('w', myfunction);     varsample2.trace('w', myfunction)
    
    # Sample lenght
    label1 = ttk.Label(root, text = "Enter the sample's lenght l:", justify="right")
    label1.grid(row = 4, column = 0, sticky=tk.E)

    # Sample thickness
    label2 = ttk.Label(root, text = "Enter the sample's thickness d:", justify="right")
    label2.grid(row = 4, column = 2, sticky=tk.E)
    
    # Lenght unit
    label3 = ttk.Label(root, text = "mm", justify="left")
    label3.grid(row = 5, column = 1, sticky=tk.W)
    
    # Thickness unit
    label4 = ttk.Label(root, text = "nm", justify="left")
    label4.grid(row = 5, column = 3, sticky=tk.W)

    ent4=tk.Entry(root, justify=tk.RIGHT, textvariable=varsample1)
    ent4.grid(row = 5, column = 0, sticky=tk.E, ipadx=10)
    ent5=tk.Entry(root, justify=tk.RIGHT, textvariable=varsample2)
    ent5.grid(row = 5, column = 2, sticky=tk.E, ipadx=20)

    # Continue button settings ----------------------------------------------------------
    exit_button = ttk.Button(root, text="Continue", command=root.destroy)
    exit_button.grid(row = 6, column = 0, columnspan=6)
    exit_button.config(state='disabled')

    # Stop all process if windows is closed ---------------------------------------------
    def on_closing():
        if messagebox.askyesno("Quit", "Do you want to quit?"):
            sys.exit()
    root.protocol("WM_DELETE_WINDOW", on_closing)

    # Run the window --------------------------------------------------------------------
    root.mainloop()

    # Set path name for the files accordingly to the os ---------------------------------
    if platform.system() == "Windows":
        filename1 = filename1.replace("/", "\\")
        filename2 = filename2.replace("/", "\\")
        filename3 = filename3.replace("/", "\\")
    elif platform.system() == "Linux":
        filename1 = filename1.replace(" ", "\ ")
        filename2 = filename2.replace(" ", "\ ")
        filename3 = filename3.replace(" ", "\ ")


    # Set sample parameters -------------------------------------------------------------
    lenght = varsample1.get()
    thick = varsample2.get()
    if lenght.find(",") != -1:
        lenght = lenght.replace(",", ".")
    if thick.find(",") != -1:
        thick = thick.replace(",", ".")

##<===================================================================================================>##
##                                      Method: Energy absorption                                      ##
##<===================================================================================================>##

elif choice.get() == 2:

    # Create the root window ------------------------------------------------------------
    root = tk.Tk()
    root.title('Energy Absorption method:')
    root.resizable(False, False)
    root.geometry('800x400')
    center(root)
    root.grid_rowconfigure((0,7), weight=1)
    root.grid_columnconfigure((0,1), weight=1)

    # Function that activates the "continue" button -------------------------------------
    def myfunction(*event):
        if len(ent1.get()) > 1 and len(ent2.get()) > 1 and len(ent3.get()) > 1 and len(ent4.get()) > 1:
            exit_button.config(state='normal')
        else:
            exit_button.config(state='disabled')

    # Introduction ----------------------------------------------------------------------
    label = ttk.Label(root, text = "For this method 4 files are necessary: S11 and S21 for calibration "
                                    "and S11 and S21 of the sample.\n"
                                    "\nChoose the files to be analyzed:", 
                                    justify=CENTER, 
                                    wraplength=700)
    label.grid(row = 0, column = 0, columnspan=2)

    # Entry variables -------------------------------------------------------------------
    var = tk.StringVar(root); var2 = tk.StringVar(root); var3 = tk.StringVar(root); var4 = tk.StringVar(root)
    var.trace('w', myfunction); var2.trace('w', myfunction); var3.trace('w', myfunction); var4.trace('w', myfunction)

    # File location dialog --------------------------------------------------------------
    # ent1 -> Sample S11
    ent1=tk.Entry(root, justify=CENTER, textvariable=var, state=tk.DISABLED)
    ent1.grid(row = 2, column = 0, ipadx=140)
    ent1.config(disabledforeground="black",disabledbackground="white")

    # ent2 -> Sample S21
    ent2=tk.Entry(root, justify=CENTER, textvariable=var2, state=tk.DISABLED)
    ent2.grid(row = 3, column = 0, ipadx=140)
    ent2.config(disabledforeground="black",disabledbackground="white")
    
    # ent3 -> Calibration S11
    ent3=tk.Entry(root, justify=CENTER, textvariable=var3, state=tk.DISABLED)
    ent3.grid(row = 5, column = 0, ipadx=140)
    ent3.config(disabledforeground="black",disabledbackground="white")

    # ent4 -> Calibration S21
    ent4=tk.Entry(root, justify=CENTER, textvariable=var4, state=tk.DISABLED)
    ent4.grid(row = 6, column = 0, ipadx=140)
    ent4.config(disabledforeground="black",disabledbackground="white")

    # Button definitions ----------------------------------------------------------------
    def select_S11Sample():
        global filename1
        filename1 = fd.askopenfilename(title='Choose the S11 sample file')
        ent1.config(state=tk.NORMAL)
        ent1.delete(0, tk.END)
        ent1.insert(tk.END, filename1)
        ent1.config(state=tk.DISABLED)

    def select_S21Sample():
        global filename2
        filename2 = fd.askopenfilename(title='Choose the S21 sample file')
        ent2.config(state=tk.NORMAL)
        ent2.delete(0, tk.END)
        ent2.insert(tk.END, filename2)
        ent2.config(state=tk.DISABLED)

    def select_S11Cavity():
        global filename3
        filename3 = fd.askopenfilename(title='Choose the S11 calibration file')
        ent3.config(state=tk.NORMAL)
        ent3.delete(0, tk.END)
        ent3.insert(tk.END, filename3)
        ent3.config(state=tk.DISABLED)

    def select_S21Cavity():
        global filename4
        filename4 = fd.askopenfilename(title='Choose the S21 calibration file')
        ent4.config(state=tk.NORMAL)
        ent4.delete(0, tk.END)
        ent4.insert(tk.END, filename4)
        ent4.config(state=tk.DISABLED)


    # Open file buttons -----------------------------------------------------------------
    # Sample
    label2 = ttk.Label(root, text = "Files of the sample:", 
                                    justify="left", 
                                    wraplength=700)
    label2.grid(row = 1, column = 1)

    Samplefile = ttk.Button(
        root,
        text="Choose the S11 file",
        command=select_S11Sample,
        width = 27
    )
    Samplefile.grid(row = 2, column = 1, pady=10)

    Substratefile = ttk.Button(
        root,
        text='Choose the S21 file',
        command=select_S21Sample,
        width = 27
    )
    Substratefile.grid(row = 3, column = 1, pady=10)
    
    # Cavity
    label3 = ttk.Label(root, text = "Files for calibration:", 
                                    justify="left", 
                                    wraplength=700)
    label3.grid(row = 4, column = 1)

    Holderfile = ttk.Button(
        root,
        text='Choose the S11 file',
        command=select_S11Cavity,
        width = 27
    )
    Holderfile.grid(row = 5, column = 1, pady=10)

    Holderfile = ttk.Button(
        root,
        text='Choose the S21 file',
        command=select_S21Cavity,
        width = 27
    )
    Holderfile.grid(row = 6, column = 1, pady=10)

    # Continue button settings ----------------------------------------------------------
    exit_button = ttk.Button(root, text="Continue", command=root.destroy)
    exit_button.grid(row = 7, column = 0, columnspan=2)
    exit_button.config(state='disabled')

    # Stop all process if windows is closed ---------------------------------------------
    def on_closing():
        if messagebox.askyesno("Quit", "Do you want to quit?"):
            sys.exit()
    root.protocol("WM_DELETE_WINDOW", on_closing)

    # Run the window --------------------------------------------------------------------
    root.mainloop()
 
    # Set path name for the files accordingly to the os ---------------------------------
    if platform.system() == "Windows":
        filename1 = filename1.replace("/", "\\")
        filename2 = filename2.replace("/", "\\")
        filename3 = filename3.replace("/", "\\")
        filename4 = filename4.replace("/", "\\")

    elif platform.system() == "Linux":
        filename1 = filename1.replace(" ", "\ ")
        filename2 = filename2.replace(" ", "\ ")
        filename3 = filename3.replace(" ", "\ ")
        filename4 = filename4.replace(" ", "\ ")

##<===================================================================================================>##
##                                   Method: S11/Impedance variation                                   ##
##<===================================================================================================>##

else:
    # Change the title accordingly with the method chosen -------------------------------
    if choice.get() == 3:
        titl = "Variation of S11 method"
        lab = "The variation of S11"
    else:
        titl = "Magnetoimpedance method"
        lab = "The variation of impedance of S11"

    # Create the root window ------------------------------------------------------------
    root = tk.Tk()
    root.title(titl)
    root.resizable(False, False)
    root.geometry('800x200')
    center(root)
    root.grid_rowconfigure((0,2), weight=1)
    root.grid_columnconfigure((0,1), weight=1)

    # Function that activates the "continue" button -------------------------------------
    def myfunction(*event):
        if len(ent1.get()) > 1:
            exit_button.config(state='normal')
        else:
            exit_button.config(state='disabled')

    # Introduction ----------------------------------------------------------------------
    label = ttk.Label(root, text = "For this method only one file is necessary: "+lab
                                    +"\n\nChoose the file to be analyzed:", 
                                    justify=CENTER, 
                                    wraplength=700)
    label.grid(row = 0, column = 0, columnspan=2)

    # Entry variable --------------------------------------------------------------------
    var=tk.StringVar(root)
    var.trace('w', myfunction)

    # File location dialog --------------------------------------------------------------
    # Data file
    ent1=tk.Entry(root, justify=CENTER, textvariable=var, state=tk.DISABLED)
    ent1.grid(row = 1, column = 0, ipadx=140)
    ent1.config(disabledforeground="black",disabledbackground="white")

    # Button definitions ----------------------------------------------------------------
    def select_fileSample():
        global filename1
        filename1 = fd.askopenfilename(title='Choose file')
        ent1.config(state=tk.NORMAL)
        ent1.delete(0, tk.END)
        ent1.insert(tk.END, filename1)
        ent1.config(state=tk.DISABLED)

    # Open file buttons -----------------------------------------------------------------
    Samplefile = ttk.Button(
        root,
        text="Choose the sample file",
        command=select_fileSample,
        width = 27
    )
    Samplefile.grid(row = 1, column = 1, pady=10)

    # Continue button settings ----------------------------------------------------------
    exit_button = ttk.Button(root, text="Continue", command=root.destroy)
    exit_button.grid(row = 2, column = 0, columnspan=2)
    exit_button.config(state='disabled')

    # Stop all process if windows is closed ---------------------------------------------
    def on_closing():
        if messagebox.askyesno("Quit", "Do you want to quit?"):
            sys.exit()
    root.protocol("WM_DELETE_WINDOW", on_closing)

    # Run the window --------------------------------------------------------------------
    root.mainloop()

    # Set path name for the file accordingly to the os ---------------------------------
    if platform.system() == "Windows":
        filename1 = filename1.replace("/", "\\")

    elif platform.system() == "Linux":
        filename1 = filename1.replace(" ", "\ ")


##<===================================================================================================>##
##                                      Running the chosen method                                      ##
##<===================================================================================================>##

# Create root window --------------------------------------------------------------------
root = tk.Tk()
root.geometry('300x120')
root.title('')
center(root)
root.grid()

# Progressbar settings ------------------------------------------------------------------
pb = ttk.Progressbar(
    root,
    orient='horizontal',
    mode='indeterminate',
    length=280
)
# Progressbar location
pb.grid(column=0, row=0, columnspan=2, padx=10, pady=20)

# Multithreading for the progressbar ----------------------------------------------------
def run_thread(method):
    Thread(target=run_function, args=[method]).start()

# Running the chosen method -------------------------------------------------------------
def run_function(method):
    # Windows widgets
    pb.start(interval=10)
    label2 = ttk.Label(root, text = "Processing...", 
                                    justify=CENTER, 
                                    wraplength=700)
    label2.grid(row = 1, columnspan=2)

    # Permeability program call ---------------------------------------------------------
    if method == 1:
        if platform.system() == "Windows":
            exec = path+"\\Perm.exe"
            args = "\"{0}\" \"{1}\" \"{2}\" {3} {4}".format(filename1, filename2, filename3, lenght, thick)
            sub.run("{0} {1}".format(exec, args))
        elif platform.system() == "Linux":
            sub.run(path+"Methods/Perm.out {0} {1} {2} {3} {4}".format(filename1, filename2, filename3, lenght, thick), shell=True)
            
    # Absorption program call -----------------------------------------------------------
    elif method == 2:
        if platform.system() == "Windows":
            exec = path+"\\Abs.exe"
            args = "\"{0}\" \"{1}\" \"{2}\" \"{3}\"".format(filename1, filename2, filename3, filename4)
            sub.run("{0} {1}".format(exec, args))
        elif platform.system() == "Linux":
            sub.run(path+"Methods/Abs.out {0} {1} {2} {3}".format(filename1, filename2, filename3, filename4), shell=True)
    
    # Variation program call ------------------------------------------------------------
    else:
        if platform.system() == "Windows":
            exec = path+"\\Delta.exe"
            args = "{0}".format(filename1)
            sub.run("{0} \"{1}\"".format(exec, args))
        elif platform.system() == "Linux":
            sub.run(path+"Methods/Delta.out {0}".format(filename1), shell=True)

    # Delete label and progressbar ------------------------------------------------------
    pb.destroy()
    label2.destroy()

    # Ending message --------------------------------------------------------------------
    root.grid_rowconfigure((0,1), weight=1)
    root.grid_columnconfigure(0, weight=1)
    label = ttk.Label(root, text = "Done!", 
                                    justify=CENTER, 
                                    wraplength=700)
    label.grid(row = 0)

    # Exit button settings --------------------------------------------------------------
    exit_button = ttk.Button(root, text="Exit", command=root.destroy)
    exit_button.grid(row = 1)

# Multithreading function to run the analysis method while displaying progressbar -------
run_thread(choice.get())

# Run the windows -----------------------------------------------------------------------
root.mainloop()
