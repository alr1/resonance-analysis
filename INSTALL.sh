#!/bin/bash

path=$(pwd)
path2=${path//[ ]/\\ }

sudo mv ./Codes/Resonance\ Analysis.py .
echo "gio set Resonance\ Analysis.py metadata::custom-icon file://${path2}/Icon.svg" | bash
chmod +x Resonance\ Analysis.py

mkdir Methods

g++ Codes/AbsorcaoCal.cpp -o ./Methods/Abs.out
g++ Codes/Delta.cpp -o ./Methods/Delta.out
g++ Codes/Permeabilidade.cpp -o ./Methods/Perm.out

echo "[Desktop Entry]
Name=Resonance Anaylsis
Version=1.0
Icon=${path}/Icon.svg
Exec=python3 ${path2}/Resonance\ Analysis.py
Terminal=false
Type=Application" >> ~/.local/share/applications/Analysis.desktop
