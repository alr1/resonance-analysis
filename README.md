# Ferromagnetic Resonance Analysis
This program aims to analyze Vector Network Analyzer (VNA) files using different methods to obtain ferromagnetic resonance (FMR) data of films, like the dispersion relation and resonance linewidth.

## Available methods
The available methods are:
* Complex magnetic permeability -> Uses the V. Bekker et al. (DOI: [10.1016/j.jmmm.2003.08.033](https://doi.org/10.1016/j.jmmm.2003.08.033)) model to obtain resonance data;
* Energy Absorption -> Uses the absorption of energy, as described by David Pozar on his book "Microwave Engineering", to detect resonance;
* Impedance variation -> Uses the relation between magnetoimpedance and FMR to obtain the resonance data. The same method can be applied to the variation of S11.

A theoretical review and description of the experimental procedures of the three methods, with references, can be found (in Portuguese) on my graduate term paper: https://lume.ufrgs.br/handle/10183/206641.

### File format
For all methods, files with data measured by a VNA in the following format will be required:
```
frequency(Hz)    field(Oe)    real    imag    mod 
10000000, -300, -0.08026079088, -0.02146913856, 0.08308260024332152
17997997.997998, -300, 0.005884153303, 0.01552571915, 0.01660334951803047
...
```

### To do
From Bekker's model it is possible to obtain the relative complex permeability, but it is not implemented yet on this program. The results that are included on the outputs named as relative permeability are placeholders.

# Installing
The easiest way to use the program is to just download the correct binary for your OS in the releases page and use it, but if you want to build it from source, here is how:
## Linux
### Prerequisites
* gcc/g++ 
* Python3
* git (optional)
* Pyinstaller (optional)

### Building
Start by cloning this git with
```
git clone https://gitlab.com/alr1/resonance-analysis
```
Alternatively, you can simply download this git as a .zip and unpack it wherever you like.

Now you have two options: use pyinstaller and create a binary or just keep using it as an executable python script.
If you don't want to use pyinstaller, just allow executing INSTALL.sh as a program and run it:
```
chmod +x INSTALL.sh
./INSTALL.sh
```
This will create a shortcut in your applications' menu. Just execute "Resonance Analysis.py" as a program, and you are done.
If instead you want to build a binary with pyinstaller, run, inside the "resonance-analysis" directory, the following commands:
```
mkdir Methods
g++ Codes/AbsorcaoCal.cpp -o ./Methods/Abs.out
g++ Codes/Delta.cpp -o ./Methods/Delta.out
g++ Codes/Permeabilidade.cpp -o ./Methods/Perm.out
mv Codes/Resonance\ Analysis.py .
```
Then run pyinstaller with the commands:
```
pyinstaller --onefile --icon=Icon.svg './Resonance Analysis.py' --add-data './Methods/Abs.out:./Methods/' --add-data './Methods/Delta.out:./Methods/' --add-data './Methods/Perm.out:./Methods/' --add-data './libstdc++-6.dll:.' --clean --noconsole
```
Your binary will be created inside the "dist" directory. You can move it for wherever you like!

## Windows

* Obs.: Your antivirus may detect a false positive and block the c++ algorithms from running. To solve this, you will need to allow this software to run on your antivirus configs.

### Prerequisites
* gcc/g++ (MinGW from MSYS2 is suggested)
* Python3
* Pyinstaller

### Building
Download the .zip of this git and extract it wherever you prefer.
Inside the unzipped folder create a folder with the name "Methods".
Inside the folder "Codes" open the PowerShell (shift+right click will open a dialog with the option to open it) and run the following commands:
```
g++ AbsorcaoCal.cpp -o Abs.exe -mwindows
g++ Permeabilidade.cpp -o Perm.exe -mwindows
g++ Delta.cpp -o Delta.exe -mwindows
```
Move the created .exe files to the "Methods" folder inside the parent directory.
Move "Resonance Analysis.py" from inside the "Codes" folder to the parent directory.
Run pyinstaller with the following commands:
```
pyinstaller --onefile --icon=Icon.ico '.\Resonance Analysis.py' --add-data '.\Methods\Abs.exe;.' --add-data '.\Methods\Delta.exe;.' --add-data '.\Methods\Perm.exe;.' --add-data '.\libstdc++-6.dll;.' --clean --noconsole
```
Your binary is finally created inside the "dist" folder. You can move it to wherever you like!